import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QToolTip,
                             QPushButton, QApplication, QComboBox, QMessageBox)
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtWidgets import QDesktopWidget
from PyQt5.QtCore import QCoreApplication


class emptyObj(object):
    def __init__(self):
        pass


class appMain(QWidget):
    def __init__(self):
        super().__init__()

        QToolTip.setFont(QFont('SansSerif', 10))
        resolution = QDesktopWidget().screenGeometry()

        # Задание Const
        self.CONSTS = emptyObj()
        self.CONSTS.width = 200
        self.CONSTS.height = 100
        self.CONSTS.pading = 15
        self.CONSTS.PositionX = int((resolution.width() / 2) - (self.CONSTS.width / 2))
        self.CONSTS.PositionY = int((resolution.height() / 2) - (self.CONSTS.height / 2))

        # Форма приложения
        self.setGeometry(self.CONSTS.PositionX, self.CONSTS.PositionY, self.CONSTS.width, self.CONSTS.height)
        self.setWindowTitle('KingApp')
        self.setWindowIcon(QIcon('img/faviconnew.ico'))
        self.setToolTip('Это <b>Форма</b>. И только.')
        self.setWindowFlags(Qt.MSWindowsFixedSizeDialogHint) # Делаем форму фиксированного размера

        # Список ППЭ
        PPE_List = QComboBox(self)
        PPE_List.resize(self.CONSTS.width - self.CONSTS.pading*2 , 22)
        PPE_List.addItem("Ubuntu")
        PPE_List.addItem("Mandriva")
        PPE_List.move(self.CONSTS.pading, self.CONSTS.pading)

        # Кнопка Скачивания Ключа.
        applyBtn = QPushButton('Подтвердить', self)
        applyBtn.setToolTip('Это <b>Кнопка</b>. Не нажимай ее!!!!!.')
        applyBtn.clicked.connect(QCoreApplication.instance().quit)
        applyBtn.resize(applyBtn.sizeHint())
        applyBtn.move(int((self.CONSTS.width / 2) - (applyBtn.width() / 2)),
                 self.CONSTS.height - (applyBtn.height() + self.CONSTS.pading))

        self.show()

    def closeEvent(self, event):
        """
        Событие сработает при нажатии на кнопку с крестиком(Закрытия окна).
        :param event: Событие
        :return: None
        """
        msg = QMessageBox()
        # Доп инфа: http://pyqt.sourceforge.net/Docs/PyQt4/qmessagebox.html
        msg.setIcon(QMessageBox.Question)
        msg.setWindowIcon(QIcon('img/faviconnew.ico'))
        msg.setText("Вы точно хотите отменить получение ключа?")
        # msg.setInformativeText("This is additional information")
        # msg.setDetailedText("The details are as follows:")
        msg.setWindowTitle('Внимание!')
        msg.addButton(QPushButton('Да'), QMessageBox.ApplyRole)
        msg.addButton(QPushButton('Нет'), QMessageBox.NoRole)
        reply = msg.exec_()

        if reply == 0:
            event.accept()
        else:
            event.ignore()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = appMain()
    sys.exit(app.exec_())
